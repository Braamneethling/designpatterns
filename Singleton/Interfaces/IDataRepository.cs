﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singleton.Models;

namespace Singleton.Interfaces
{
    public interface IDataRepository
    {
        void AddPerson(Person person);
        void AddProduct(Product person);

        IEnumerable<Person> GetPeople();
        IEnumerable<Product> GetProducts();
    }
}
