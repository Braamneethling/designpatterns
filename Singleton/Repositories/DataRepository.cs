﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singleton.Interfaces;
using Singleton.Models;

namespace Singleton.Repositories
{
    public class DataRepository : IDataRepository
    {
        public void AddPerson(Person person)
        {
            throw new NotImplementedException();
        }

        public void AddProduct(Product product)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Person> GetPeople()
        {
            var people = new List<Person>();
            const string query = "SELECT * FROM Person";
            var command = GetSqlConnection(query);
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var person = GetPerson(reader);
                    people.Add(person);
                }
            }

            return people;
        }

        private static Person GetPerson(SqlDataReader reader)
        {
            return new Person
            {
                PersonId = Convert.ToInt32(reader["PersonId"]),
                ContactNumber = reader["ContactNumber"].ToString()
            };
        }

        public IEnumerable<Product> GetProducts()
        {
            var products = new List<Product>();
            const string query = "SELECT * FROM Products";
            var command = GetSqlConnection(query);
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    var product = GetProduct(reader);
                    products.Add(product);
                }
            }

            return products;
        }

        private static Product GetProduct(SqlDataReader reader)
        {
            return new Product
            {
                ProductId = Convert.ToInt32(reader["ProductId"]),
                ProductName = reader["ProductName"].ToString(),
                ProductExpiryDate = Convert.ToDateTime(reader["ProductExpiryDate"])
            };
        }

        private static SqlCommand GetSqlConnection(string query)
        {
            return new SqlCommand(query, Connection._instance);
        }
    }
}
