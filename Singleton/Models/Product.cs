﻿using System;

namespace Singleton.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public DateTime ProductExpiryDate { get; set; }
    }
}