﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton.Models
{
    public class Person
    {
        public int PersonId { get; set; }
        public string ContactNumber { get; set; }
    }
}
