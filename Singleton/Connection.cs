﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    public class Connection
    {
        private static object _lock = new object();
        private static readonly SqlConnection _connection;
        public static SqlConnection _instance => _connection;
        //Static to only run once at call.
        static Connection()
        {
            if (_connection == null)
            {
                lock (_lock)
                {
                    _connection = new SqlConnection(@"Server=BRAAM-PC\SQLEXPRESS;Database=Learning_Singleton;Integrated Security=SSPI;");
                    _connection.Open();
                }
            }
        }
       
    }
}
