﻿using Singleton.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singleton.Models;

namespace Singleton.Controllers
{
    public class DataController
    {
        private readonly IDataRepository _dataRepository;

        public DataController(IDataRepository dataRepository)
        {
            this._dataRepository = dataRepository;
        }

        public IEnumerable<Person> GetPeople()
        {
           return _dataRepository.GetPeople();
        }

        public IEnumerable<Product> GetProducts()
        {
            return _dataRepository.GetProducts();
        }
    }
}
