﻿using System;
using NUnit.Framework;
using Strategy.DamageTypes;
using Strategy.Weapons;

namespace Strategy.Tests
{
    [TestFixture]
    public class StrategyTests
    {
        public class FireDamageTests
        {
            [Test]
            public void DoDamage_GivenFireDamageBySword_ShouldDeductHealth()
            {
                //arrange
                PlayerStats.Target.Health = 100;
                var weaponBase = new Sword(30, new FireDamage());
                //act
                weaponBase.TryToAttack();
                //assert
                Assert.AreEqual(PlayerStats.TotalDamage, 35);
                Assert.AreEqual(PlayerStats.Target.Health, 65);
            }

            [Test]
            public void DoDamage_GivenFireDamageByAxe_ShouldDeductHealth()
            {
                //arrange
                PlayerStats.Target.Health = 100;
                var weaponBase = new Axe(35, new FireDamage());
                //act
                weaponBase.TryToAttack();
                //assert
                Assert.AreEqual(PlayerStats.TotalDamage, 40);
                Assert.AreEqual(PlayerStats.Target.Health, 60);
            }

            [Test]
            public void DoDamage_GivenFireDamageByDagger_ShouldDeductHealth()
            {
                //arrange
                PlayerStats.Target.Health = 100;
                var weaponBase = new Dagger(15, new FireDamage());
                //act
                weaponBase.TryToAttack();
                //assert
                Assert.AreEqual(PlayerStats.TotalDamage, 20);
                Assert.AreEqual(PlayerStats.Target.Health, 80);
            }
        }

        public class IceDamageTests
        {
            [Test]
            public void DoDamage_GivenIceDamageBySword_ShouldDeductHealth()
            {
                //arrange
                PlayerStats.Target.Health = 100;
                var weaponBase = new Sword(30, new IceDamage());
                //act
                weaponBase.TryToAttack();
                //assert
                Assert.AreEqual(PlayerStats.TotalDamage, 33);
                Assert.AreEqual(PlayerStats.Target.Health, 67);
            }

            [Test]
            public void DoDamage_GivenIceDamageByAxe_ShouldDeductHealth()
            {
                //arrange
                PlayerStats.Target.Health = 100;
                var weaponBase = new Axe(35, new IceDamage());
                //act
                weaponBase.TryToAttack();
                //assert
                Assert.AreEqual(PlayerStats.TotalDamage, 38);
                Assert.AreEqual(PlayerStats.Target.Health, 62);
            }

            [Test]
            public void DoDamage_GivenIceDamageByDagger_ShouldDeductHealth()
            {
                //arrange
                PlayerStats.Target.Health = 100;
                var weaponBase = new Dagger(15, new IceDamage());
                //act
                weaponBase.TryToAttack();
                //assert
                Assert.AreEqual(PlayerStats.TotalDamage, 18);
                Assert.AreEqual(PlayerStats.Target.Health, 82);
            }
        }
        
    }
}
