﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Models
{
    public class WeatherDetails
    {
        public double Temperature { get; set; }
        public int WindSpeed { get; set; }
        public string SunStatus { get; set; }
    }
}
