﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Helpers;
using Facade.Models;
using Facade.Services;

namespace Facade
{
    public class WeatherFacadeController
    {
        private readonly WeatherService _weatherService;
        private readonly UnitConverter _unitConverter;

        public WeatherFacadeController(WeatherService weatherService, UnitConverter unitConverter)
        {
            _weatherService = weatherService;
            _unitConverter = unitConverter;
        }
        public WeatherDetails GetCityInformation(string city)
        {
            var weatherDetails = _weatherService.GetWeatherDetails(city);
            weatherDetails.Temperature = _unitConverter.FahrenheitToCelcius(weatherDetails.Temperature);
            return weatherDetails;
        }
    }
}
