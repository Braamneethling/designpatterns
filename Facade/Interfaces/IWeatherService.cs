﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Models;

namespace Facade.Interfaces
{
    public interface IWeatherService
    {
        WeatherDetails GetWeatherDetails(string city);
    }
}
