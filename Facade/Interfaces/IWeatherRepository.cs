﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Models;

namespace Facade.Interfaces
{
    public interface IWeatherRepository
    {
        WeatherDetails GetWeatherDetails(string city);
    }
}
