﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Helpers;
using Facade.Interfaces;
using Facade.Models;

namespace Facade.Services
{

    public class WeatherService : IWeatherService
    {
        private readonly IWeatherRepository _weatherRepository;

        public WeatherService(IWeatherRepository weatherRepository)
        {
            _weatherRepository = weatherRepository;
        }
        public WeatherDetails GetWeatherDetails(string city)
        {
            return _weatherRepository.GetWeatherDetails(city);
        }
    }
}
