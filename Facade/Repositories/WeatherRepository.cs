﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Interfaces;
using Facade.Models;

namespace Facade.Repositories
{
    public class WeatherRepository : IWeatherRepository
    {
        public WeatherDetails GetWeatherDetails(string city)
        {
            //any data because i am mocking it out in my tests
            return new WeatherDetails
            {
                SunStatus = "Sunny",
                Temperature = 30,
                WindSpeed = 17
            };
        }
    }
}
