﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.Interfaces;

namespace DesignPatterns
{
    //One of the Behavioral Design Patterns
    //Strategy lets the algorithm vary independently from the client that uses it. Decoupling the algorithm from the one using the algorithm
    public partial class StrategyPattern
    {
        
        public class Duck
        {
            public string Quack()
            {
                return "Quack";
            }

            public string Display()
            {
                return "";
            }

            public string Fly()
            {
                return "Takes off for flight";
            }
        }

        public class WildDuck : Duck
        {
            public new string Display()
            {
                return "this is a wild duck";
            }
        }

        public class CityDuck : Duck
        {
            public new string Display()
            {
                return "this is a city duck";
            }
        }

        public class RubberDuck : Duck
        {
            public new string Display()
            {
                return "this is a rubber duck";
            }
        }
    }
}
