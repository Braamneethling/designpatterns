﻿using DesignPatterns.Interfaces;

namespace DesignPatterns
{
    public partial class StrategyPattern
    {
        public class SimpleQuack : IQuackBehaviour
        {
            public string Quack()
            {
                return "simple quack";
            }
        }

        public class NoQuack : IQuackBehaviour
        {
            public string Quack()
            {
                return "....";
            }
        }
    }
}