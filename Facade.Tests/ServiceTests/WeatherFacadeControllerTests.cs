﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Helpers;
using Facade.Interfaces;
using Facade.Models;
using Facade.Repositories;
using Facade.Services;
using Facade.Tests.Builders;
using Moq;
using NUnit.Framework;
using FluentAssertions;

namespace Facade.Tests.ServiceTests
{
    [TestFixture]
    public class WeatherFacadeControllerTests
    {
        [Test]
        public void GetCityInformation_GivenCallToGetWeatherDetails_ShouldReturnCorrectMockedDetails()
        {
            //arrange
            var weatherService = new Mock<WeatherService>();
            var unitConverter = new UnitConverter();

            var expectedWeatherDetails = new WeatherDetailsBuilder().WithSunStatus("Sunny").WithTemperature(24).WithWindSpeed(17).Build();
            weatherService.Setup(x => x.GetWeatherDetails("")).Returns(expectedWeatherDetails);

            var sut = BuildWindService(weatherService.Object, unitConverter);
            //act
            var weatherDetails = sut.GetCityInformation("durban");
            //assert
            weatherDetails.Should().NotBeNull();
            weatherDetails.WindSpeed.Should().Be(expectedWeatherDetails.WindSpeed);
            weatherDetails.Temperature.Should().Be(expectedWeatherDetails.Temperature);
            weatherDetails.SunStatus.Should().Be(expectedWeatherDetails.SunStatus);

        }

        private static WeatherFacadeController BuildWindService(WeatherService weatherService, UnitConverter unitConverter)
        {
            return new WeatherFacadeController(weatherService, unitConverter);
        }
    }
}
