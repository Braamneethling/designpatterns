﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Interfaces;
using Facade.Models;
using Facade.Repositories;
using Facade.Services;
using Facade.Tests.Builders;
using Moq;
using NUnit.Framework;
using FluentAssertions;

namespace Facade.Tests.ServiceTests
{
    [TestFixture]
    public class WeatherServiceTests
    {
        [Test]
        public void GetWeatherDetails_GivenCallToGetWeatherDetails_ShouldReturnCorrectMockedDetails()
        {
            //arrange
            var weatherRepo = new Mock<IWeatherRepository>();
            var expectedWeatherDetails = new WeatherDetailsBuilder().WithSunStatus("Sunny").WithTemperature(24).WithWindSpeed(17).Build();
            weatherRepo.Setup(x => x.GetWeatherDetails("durban")).Returns(expectedWeatherDetails);

            var sut = BuildWindService(weatherRepo);
            //act
            var weatherDetails = sut.GetWeatherDetails("durban");
            //assert
            weatherDetails.Should().NotBeNull();
            weatherDetails.WindSpeed.Should().Be(expectedWeatherDetails.WindSpeed);
            weatherDetails.Temperature.Should().Be(expectedWeatherDetails.Temperature);
            weatherDetails.SunStatus.Should().Be(expectedWeatherDetails.SunStatus);

        }

        private static WeatherService BuildWindService(IMock<IWeatherRepository> weatherRepo)
        {
            return new WeatherService(weatherRepo.Object);
        }
    }
}
