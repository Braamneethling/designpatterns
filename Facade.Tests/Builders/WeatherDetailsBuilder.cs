﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Facade.Models;
using PeanutButter.RandomGenerators;

namespace Facade.Tests.Builders
{
    public class WeatherDetailsBuilder : GenericBuilder<WeatherDetailsBuilder, WeatherDetails>
    {
        private readonly WeatherDetails _weatherDetails = new WeatherDetails();
        public WeatherDetailsBuilder WithWindSpeed(int windSpeed)
        {
            _weatherDetails.WindSpeed = windSpeed;
            return this;
        }

        public WeatherDetailsBuilder WithSunStatus(string sunStatus)
        {
            _weatherDetails.SunStatus = sunStatus;
            return this;
        }

        public WeatherDetailsBuilder WithTemperature(double temperature)
        {
            _weatherDetails.Temperature = temperature;
            return this;
        }

        public override WeatherDetails Build()
        {
            return _weatherDetails;
        }
    }
}
