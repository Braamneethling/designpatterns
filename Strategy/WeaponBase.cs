﻿using Strategy.Interfaces;

namespace Strategy
{
    public class WeaponBase
    {
        public int damage = 0;
        public IDoDamage damageType;

        public void TryToAttack()
        {
            damageType.DoDamage(damage, damageType);
        }
    }
}