﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Interfaces;

namespace Strategy.Weapons
{
    public class Dagger : WeaponBase
    {
        public Dagger(int damage, IDoDamage damageType)
        {
            this.damage = damage;
            this.damageType = damageType;
        }
    }
}
