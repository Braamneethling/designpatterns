﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Interfaces;

namespace Strategy.Weapons
{
    public class Axe : WeaponBase
    {
        public Axe(int damage, IDoDamage damageType)
        {
            this.damage = damage;
            this.damageType = damageType;
        }
    }
}
