﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strategy.Interfaces;

namespace Strategy.Weapons
{
    public class Sword : WeaponBase
    {
        public Sword(int damage, IDoDamage damageType)
        {
            this.damage = damage;
            this.damageType = damageType;
        }
    }
}
