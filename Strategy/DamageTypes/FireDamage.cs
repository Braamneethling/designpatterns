﻿
using Strategy.Interfaces;

namespace Strategy.DamageTypes
{
    public class FireDamage : IDoDamage
    {
        public void DoDamage(int damage, IDoDamage damageType)
        {
            //extra fire damage
            damage += 5;
            PlayerStats.TotalDamage += damage;
            PlayerStats.Target.Health -= damage;
        }
    }
}