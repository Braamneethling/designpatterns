﻿
using Strategy.Interfaces;

namespace Strategy.DamageTypes
{
    public class IceDamage : IDoDamage
    {
        public void DoDamage(int damage, IDoDamage damageType)
        {
            //extra ice damage
            damage += 3;
            PlayerStats.TotalDamage += damage;
            PlayerStats.Target.Health -= damage;
        }
    }
}