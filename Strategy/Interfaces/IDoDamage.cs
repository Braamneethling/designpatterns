﻿namespace Strategy.Interfaces
{
    public interface IDoDamage
    {
        void DoDamage(int damage, IDoDamage damageType);
    }
}
