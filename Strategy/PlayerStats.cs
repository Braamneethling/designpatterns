﻿namespace Strategy
{
    public static class PlayerStats
    {
        public static int TotalDamage { get; set; }

        public static class Target
        {
            public static int Health
            {
                get;
                set;
            }
        }
    }
}