﻿namespace DesignPatterns.Interfaces
{
    public interface IQuackBehaviour
    {
        string Quack();
    }
}
