﻿namespace DesignPatterns.Interfaces
{
    public interface IFlyBehaviour
    {
        string Fly();
    }
}
