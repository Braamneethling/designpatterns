﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Singleton.Controllers;
using Singleton.Interfaces;
using Singleton.Models;
using Singleton.Repositories;
using Singleton.Tests.Builders;

namespace Singleton.Tests
{
    [TestFixture]
    public class SingletonTests
    {
        public class GetPersonTests
        {
            [Test]
            public void GetPeople_ShouldReturn1PersonRecord()
            {
                //arrange
                var dataRepository = new Mock<IDataRepository>();
                var person = new PersonBuilder().WithRandomProps().Build();
                BuildDataRepositoryWithPeople(dataRepository, new List<Person> { person });
                var sut = BuildDataController(dataRepository);
                //act
                var result = sut.GetPeople().ToList();
                //assert
                result.Should().NotBeNull();
                result.Count().Should().Be(1);
            }

            [Test]
            public void GetPeople_ShouldReturn2PersonRecords()
            {
                //arrange
                var dataRepository = new Mock<IDataRepository>();
                var person1 = new PersonBuilder().WithRandomProps().Build();
                var person2 = new PersonBuilder().WithRandomProps().Build();
                BuildDataRepositoryWithPeople(dataRepository, new List<Person> { person1, person2 });
                var sut = BuildDataController(dataRepository);
                //act
                var result = sut.GetPeople().ToList();
                //assert
                result.Should().NotBeNull();
                result.Count().Should().Be(2);
            }

            private static DataController BuildDataController(IMock<IDataRepository> dataRepository)
            {
                return new DataController(dataRepository.Object);
            }

            private static IDataRepository BuildDataRepositoryWithPeople(Mock<IDataRepository> dataRepository, IEnumerable<Person> people)
            {
                dataRepository.Setup(x => x.GetPeople()).Returns(people);
                return dataRepository.Object;
            }
        }

        public class ProductTests
        {
            [Test]
            public void GetProducts_ShouldReturn1ProductRecord()
            {
                //arrange
                var dataRepository = new Mock<IDataRepository>();
                var product = new ProductBuilder().WithRandomProps().Build();
                BuildDataRepositoryWithPeople(dataRepository, new List<Product> { product });
                var sut = BuildDataController(dataRepository);
                //act
                var result = sut.GetProducts().ToList();
                //assert
                result.Should().NotBeNull();
                result.Count().Should().Be(1);
            }

            [Test]
            public void GetProducts_ShouldReturn2ProductRecords()
            {
                //arrange
                var dataRepository = new Mock<IDataRepository>();
                var product1 = new ProductBuilder().WithRandomProps().Build();
                var product2 = new ProductBuilder().WithRandomProps().Build();
                BuildDataRepositoryWithPeople(dataRepository, new List<Product> { product1, product2 });
                var sut = BuildDataController(dataRepository);
                //act
                var result = sut.GetProducts().ToList();
                //assert
                result.Should().NotBeNull();
                result.Count().Should().Be(2);
            }

            private static DataController BuildDataController(IMock<IDataRepository> dataRepository)
            {
                return new DataController(dataRepository.Object);
            }

            private static IDataRepository BuildDataRepositoryWithPeople(Mock<IDataRepository> dataRepository, IEnumerable<Product> product)
            {
                dataRepository.Setup(x => x.GetProducts()).Returns(product);
                return dataRepository.Object;
            }
        }

        public class DataRepositoryTests
        {
            [Test]
            public void ShouldReturnRowsForPeople()
            {
                //Arrange
                var dataRepo = new DataRepository();
                //Act
                var result = dataRepo.GetPeople();
                //Assert
                result.Should().NotBeNull();
            }

            [Test]
            public void ShouldReturnRowsForProducts()
            {
                //Arrange
                var dataRepo = new DataRepository();
                //Act
                var result = dataRepo.GetProducts();
                //Assert
                result.Should().NotBeNull();
            }
        }

    }
}
