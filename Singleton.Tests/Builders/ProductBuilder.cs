﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PeanutButter.RandomGenerators;
using Singleton.Models;

namespace Singleton.Tests.Builders
{
    public class PersonBuilder : GenericBuilder<PersonBuilder, Person>
    {
        private readonly Person _person = new Person();
        private PersonBuilder WithId(int id)
        {
            _person.PersonId = id;
            return this;
        }

        private PersonBuilder WithContactNumber(string contactNumber)
        {
            _person.ContactNumber = contactNumber;
            return this;
        }

        public override Person Build()
        {
            return _person;
        }
    }
}
