﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PeanutButter.RandomGenerators;
using Singleton.Models;

namespace Singleton.Tests.Builders
{
    public class ProductBuilder : GenericBuilder<ProductBuilder, Product>
    {
        private readonly Product _product = new Product();
        private ProductBuilder WithId(int id)
        {
            _product.ProductId = id;
            return this;
        }

        private ProductBuilder WithContactNumber(string name)
        {
            _product.ProductName = name;
            return this;
        }

        private ProductBuilder WithExpiryDate(DateTime expiryDate)
        {
            _product.ProductExpiryDate = expiryDate;
            return this;
        }

        public override Product Build()
        {
            return _product;
        }
    }
}
