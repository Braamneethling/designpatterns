﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Composite.Base;

namespace Composite.Models
{
    public class File : IStructure
    {
        public IStructure Parent { get; set; }
        public string Name { get; set; }
        public string GetPath()
        {
            return GetParentPath(Parent, Name);
        }

        private string GetParentPath(IStructure structure, string path)
        {
            var p = path;
            if (structure == null) return p;
            p = $"{structure.Name}/{p}";
            structure = structure.Parent;
            if (structure != null)
                p = GetParentPath(structure, p);
            return p;
        }
    }
}
