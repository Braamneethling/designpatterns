﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Composite.Base;

namespace Composite.Models
{
    public class Directory : IStructure
    {
        private List<IStructure> _content;

        public Directory()
        {
            
        }

        public Directory(string name)
        {
            _content = new List<IStructure>();
            Name = name;
        }
        public IStructure Parent { get; set; }
        public string Name { get; set; }
        public string GetPath()
        {
            return GetParentPath(Parent, Name);
        }

        public void Add(IStructure structure)
        {
            structure.Parent = this;
            _content.Add(structure);
        }

        private string GetParentPath(IStructure structure,  string path)
        {
            var p = path;
            if (structure == null) return p;
            p = $"{structure.Name}/{p}";
            structure = structure.Parent;
            if (structure != null) 
                p = GetParentPath(structure, p);
            return p;
        }
    }
}
